# TP2

    SET linesize 3000
    rlwrap sqlplus oracle as sysdba
    ssh baherreros@tp-zz3-baherreros.local.isima.fr
    sudo su oracle

### 1

## 1.1

    SELECT name, open_mode FROM  V$PDBS;

    NAME								 OPEN_MODE
    --------------------------------------------------
    PDB$SEED						     READ ONLY
    ORCLPDB1							 MOUNTED

## 1.2

    ALTER PLUGGABLE DATABASE ORCLPDB1 OPEN READ WRITE;

    Pluggable database altered.
    
## 1.3

    SHOW con_name

    CON_NAME
    ------------------------------
    CDB$ROOT

    SHOW user
    USER is "SYS"

    SQL> SELECT COUNT(*) FROM ALL_USERS;

    COUNT(*)
    ----------
        35

## 1.4

    ALTER SESSION SET CONTAINER = orclpdb1 ;

    SHOW con_name

    CON_NAME
    ------------------------------
    ORCLPDB1

    SHOW user
    USER is "SYS"

    SQL> SELECT COUNT(*) FROM ALL_USERS;

    COUNT(*)
    ----------
        36


    Un user qui n'est que dans les pluggable database n'est pas dans "common" qui est la racine

    SQL> SELECT username FROM ALL_USERS WHERE common='NO';

    USERNAME
    -----------------------------------------------------------------------------------
    PDBADMIN

## 1.5

    ALTER USER pdbadmin IDENTIFIED BY "pdbadmin";

    On ne peut pas se connecter car par défaut oracle ne se lance pas sur ORCL... et pdbadmin est seulement sur cette BDD
    
## 1.6

    cd $ORACLE_HOME/bin
    ./lsnrctl
    start
    exit
    rlwrap sqlplus oracle as sysdba

    CONNECT sys/sys@//localhost:1521/orclpdb1 AS sysdba

    SQL> show user
    USER is "SYS"


    SQL> show con_name
    CON_NAME
    ------------------------------
    ORCLPDB1

    CONNECT pdbadmin/pdbadmin@//localhost:1521/orclpdb1

    SQL> show user
    USER is "PDBADMIN"

### 2

SQL> SELECT SID, username FROM V$SESSION WHERE username like 'SYS' OR username like 'PDBADMIN';

    SID USERNAME
    --------------------------------------------------------------------------------
    6   SYS
    38  PDBADMIN
    277 SYS
    283 SYS


## 2.8

    SELECT name, xid, status, start_date, start_time, con_id FROM V$TRANSACTION;

## 2.9

    SET TRANSACTION NAME ’ transP0 ’ ;
    SELECT name, xid, status, start_date, start_time, con_id FROM V$TRANSACTION;

    RIEN

## 2.10

    depuis S
    SQL> GRANT create any table to pdbadmin;

    depuis P
    SQL> CREATE TABLE PERSONNES(id NUMBER PRIMARY KEY, name VARCHAR(50));

    RIEN

## 2.11

    Pas de transaction

    La table est toujours là

    https://docs.oracle.com/cd/E17952_01/mysql-5.6-en/implicit-commit.html

    certaines commandes font des commits automatiquement


## 2.12


    SQL> describe personnes;
    ERROR:
    ORA-04043: object personnes does not exist


    SQL> describe pdbadmin.personnes;
    Name					   Null?    Type
    ----------------------------------------- -------- ----------------------------
    ID					   NOT NULL NUMBER
    NAME						    VARCHAR2(50)

