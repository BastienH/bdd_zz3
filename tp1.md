# TP1

    SET linesize 3000
    rlwrap sqlplus ...

## 1.1

### 1

    - pfile
    - init.ora

    /opt/oracle/product/18c/dbhome_1/srvm/admin/init.ora

    /opt/oracle/product/18c/dbhome_1/dbs/init.ora

    - spfile

    /opt/oracle/product/18c/dbhome_1/dbs/spfileORCLCDB.ora



### 2

#### 2.1

    db_name='ORCL'


#### 2.2 

    db_block_size=8192

#### 2.3

    remote_login_passwordfile='EXCLUSIVE'

### 3

    /opt/oracle/product/18c/dbhome_1/md/admin/LoadClobs.ctl

    /opt/oracle/product/18c/dbhome_1/assistants/dbca/templates/Seed_Database.ctl

### 4

    /opt/oracle/oradata/ORCLCDB/temp01.dbf

    /opt/oracle/oradata/ORCLCDB/pdbseed/undotbs01.dbf

    /opt/oracle/oradata/ORCLCDB/pdbseed/sysaux01.dbf

    /opt/oracle/oradata/ORCLCDB/pdbseed/system01.dbf

    /opt/oracle/oradata/ORCLCDB/pdbseed/temp012019-10-02_11-39-23-059-AM.dbf

    /opt/oracle/oradata/ORCLCDB/undotbs01.dbf

    /opt/oracle/oradata/ORCLCDB/sysaux01.dbf

    /opt/oracle/oradata/ORCLCDB/users01.dbf

    /opt/oracle/oradata/ORCLCDB/system01.dbf
    
    /opt/oracle/oradata/ORCLCDB/ORCLPDB1/temp01.dbf

    /opt/oracle/oradata/ORCLCDB/ORCLPDB1/undotbs01.dbf

    /opt/oracle/oradata/ORCLCDB/ORCLPDB1/sysaux01.dbf

    /opt/oracle/oradata/ORCLCDB/ORCLPDB1/users01.dbf

    /opt/oracle/oradata/ORCLCDB/ORCLPDB1/system01.dbf


## 1.2

### 5 

    sqlplus oracle as sysdba

### 6

    SQL> startup nomount;

    ORACLE instance started.

    Total System Global Area  771747944 bytes
    Fixed Size		    8900712 bytes
    Variable Size		  612368384 bytes
    Database Buffers	  142606336 bytes
    Redo Buffers		    7872512 bytes

    SELECT instance_name FROM V$INSTANCE;

    INSTANCE_NAME
    ----------------
    ORCLCDB

### 7

    ALTER database MOUNT;

### 8 

    SQL> SELECT name FROM V$DATABASE;

    NAME
    ---------
    ORCLCDB

### 9

    SELECT name, bytes FROM V$DATAFILE;


    NAME
--------------------------------------------------------------------------------
     BYTES     BLOCKS
---------- ----------
/opt/oracle/oradata/ORCLCDB/system01.dbf
 880803840     107520

/opt/oracle/oradata/ORCLCDB/sysaux01.dbf
 534773760	65280

/opt/oracle/oradata/ORCLCDB/undotbs01.dbf
 325058560	39680


NAME
--------------------------------------------------------------------------------
     BYTES     BLOCKS
---------- ----------
/opt/oracle/oradata/ORCLCDB/pdbseed/system01.dbf
 283115520	34560

/opt/oracle/oradata/ORCLCDB/pdbseed/sysaux01.dbf
 377487360	46080

/opt/oracle/oradata/ORCLCDB/users01.dbf
   5242880	  640


NAME
--------------------------------------------------------------------------------
     BYTES     BLOCKS
---------- ----------
/opt/oracle/oradata/ORCLCDB/pdbseed/undotbs01.dbf
 104857600	12800

/opt/oracle/oradata/ORCLCDB/ORCLPDB1/system01.dbf
 283115520	34560

/opt/oracle/oradata/ORCLCDB/ORCLPDB1/sysaux01.dbf
 387973120	47360


NAME
--------------------------------------------------------------------------------
     BYTES     BLOCKS
---------- ----------
/opt/oracle/oradata/ORCLCDB/ORCLPDB1/undotbs01.dbf
 104857600	12800

/opt/oracle/oradata/ORCLCDB/ORCLPDB1/users01.dbf
   5242880	  640


11 rows selected.

SQL> SELECT name, bytes FROM V$DATAFILE;

    NAME
    ---------------------------------------
    BYTES

    /opt/oracle/oradata/ORCLCDB/system01.dbf
    880803840

    /opt/oracle/oradata/ORCLCDB/sysaux01.dbf
    534773760

    /opt/oracle/oradata/ORCLCDB/undotbs01.dbf
    325058560

    /opt/oracle/oradata/ORCLCDB/pdbseed/system01.dbf
    283115520

    /opt/oracle/oradata/ORCLCDB/pdbseed/sysaux01.dbf
    377487360

    /opt/oracle/oradata/ORCLCDB/users01.dbf
    5242880

    /opt/oracle/oradata/ORCLCDB/pdbseed/undotbs01.dbf
    104857600

    /opt/oracle/oradata/ORCLCDB/ORCLPDB1/system01.dbf
    283115520

    /opt/oracle/oradata/ORCLCDB/ORCLPDB1/sysaux01.dbf
    387973120

    /opt/oracle/oradata/ORCLCDB/ORCLPDB1/undotbs01.dbf
    104857600

    /opt/oracle/oradata/ORCLCDB/ORCLPDB1/users01.dbf
    5242880

### 10

    => 3 groupes avec 1 membre dans chaque

    SQL> SELECT l.group#, Bytes, member FROM v$LOG l JOIN V$LOGFILE lf ON l.group# = lf.group#;

         GROUP#	BYTES MEMBER

        1  209715200 /opt/oracle/oradata/ORCLCDB/redo01.log
        3  209715200 /opt/oracle/oradata/ORCLCDB/redo03.log
        2  209715200 /opt/oracle/oradata/ORCLCDB/redo02.log


### 11

    SQL>  SELECT name FROM V$CONTROLFILE;

    NAME
    /opt/oracle/oradata/ORCLCDB/control01.ctl
    /opt/oracle/oradata/ORCLCDB/control02.ctl

### 12

    SQL> SELECT version FROM V$INSTANCE;

    VERSION
    -----------------
    18.0.0.0.0

### 13

    SQL> SELECT COUNT(*) FROM V$FIXED_TABLE WHERE name LIKE 'V$%';

    COUNT(*)
    ----------
    767

### 14

    SQL> SELECT name FROM V$TABLESPACE;

    NAME
    ------------------------------
    SYSAUX
    SYSTEM
    UNDOTBS1
    USERS
    TEMP
    SYSTEM
    SYSAUX
    UNDOTBS1
    TEMP
    SYSTEM
    SYSAUX
    UNDOTBS1
    TEMP
    USERS



### 15

    ALTER DATABASE OPEN;

    SQL> SELECT OPEN_MODE FROM V$DATABASE;

    OPEN_MODE
    --------------------
    READ WRITE

### 16

    SQL> SELECT COUNT(*) FROM ALL_USERS;

    COUNT(*)
    ----------
    35

    SQL> SELECT COUNT(*) FROM DBA_ROLEs;

    COUNT(*)
    ----------
    86

## 1.3

### 17

    SQL> CREATE PFILE='myPfile' FROM SPFILE;

    SHUTDOWN

    startup pfile='/opt/oracle/product/18c/dbhome_1/dbs/myPfile'

    => Erreur

    ORA-00058: DB_BLOCK_SIZE must be 8192 to mount this database (not 16384)

### 18

    shutdown
    startup

### 19

    ALTER database CLOSE;
    ALTER database OPEN;

    ERROR at line 1:
    ORA-16196: database has been previously opened and closed

### 20

    SQL> SELECT * from V$SGAINFO;

    NAME				      BYTES RES     CON_ID
    -------------------------------- ---------- --- ----------
    Fixed SGA Size			    8900712 No		 0
    Redo Buffers			    7872512 No		 0
    Buffer Cache Size		  234881024 Yes 	 0
    In-Memory Area Size			  0     No		 0
    Shared Pool Size		  197132288 Yes 	 0
    Large Pool Size 		    8388608 Yes 	 0
    Java Pool Size			    4194304 Yes 	 0
    Streams Pool Size			  0     Yes 	 0
    Shared IO Pool Size		    8388608 Yes 	 0
    Data Transfer Cache Size		  0 Yes 	 0
    Granule Size			    4194304 No		 0
    Maximum SGA Size		  771747944 No		 0
    Free SGA Memory Available	  310378496		 0


    SELECT name, value FROM V$PARAMETER WHERE name='db_cache_size';

    NAME          VALUE
    db_cache_size 0

    ALTER SYSTEM SET DB_CACHE_SIZE='100M'

    SELECT name, value FROM V$PARAMETER WHERE name='db_cache_size';

    NAME          VALUE
    db_cache_size 104857600

### 22

    AVANT
    
    Buffer Cache Size		  234881024

    APRES

    Buffer Cache Size		  301989888

### 23

    