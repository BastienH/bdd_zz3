# TP3

    SET linesize 3000
    rlwrap sqlplus sys as sysdba
    ssh baherreros@tp-zz3-baherreros.local.isima.fr
    sudo su oracle

### 1

## 1.1

    ALTER USER sys IDENTIFIED BY "sys";

## 1.2

    SQL> SELECT authentication_type FROM DBA_USERS WHERE username = 'SYS';

    AUTHENTI
    --------
    PASSWORD

## 1.3

    /!\ PENSER A NE PAS SE METTRE SUR LA SESSION ORACLE

    ALTER SYSTEM SET remote_login_passwordfile='SHARED' scope=spfile;

    rlwrap sqlplus sys as sysdba

    STARTUP NOMOUNT

## 1.4

    ALTER SYSTEM SET remote_login_passwordfile='NONE' scope=spfile;

## 1.5

    rlwrap sqlplus sys as sysdba

    /!\ On ne peut pas se connecter

    STARTUP NOMOUNT

## 1.6

    Depuis la session oracle sys se connecte automatiquement avec l'os

## 1.7 

    LINUX !

    sudo gpasswd -d oracle dba
    sudo gpasswd -d oracle oper

    groups oracle

    ORACLE !

    ON PEUT PLUS SE CO

    sudo usermod -a -G dba oracle
    sudo usermod -a -G oper oracle


### 2

## 2.8

    SQL> STARTUP

    SQL> ALTER PLUGGABLE DATABASE ORCLPDB1 OPEN READ WRITE;

    Pluggable database altered.

    SQL> CONNECT sys/sys@//localhost:1521/orclpdb1 AS sysdba
    Connected.

    CREATE PROFILE poulidor_profile LIMIT PASSWORD_LIFE_TIME 90 FAILED_LOGIN_ATTEMPTS 8 PASSWORD_GRACE_TIME 4;

    CREATE USER poulidor IDENTIFIED BY poulidor DEFAULT TABLESPACE USERS QUOTA 800M ON USERS PROFILE poulidor_profile PASSWORD EXPIRE;

## 2.9

    CREATE ROLE connexion;
    GRANT CREATE SESSION TO connexion;

## 2.10

    CREATE ROLE basiccreators;
    GRANT CREATE ANY TABLE TO basiccreators;
    GRANT CREATE ANY INDEX TO basiccreators;
    GRANT basiccreators TO poulidor;

## 2.11

    Il ne peut pas se co car il n'a pas le le droit de créer une session

## 2.12

    GRANT connexion TO basiccreators;
    
    SQL> CONNECT poulidor/poulidor@//localhost:1521/orclpdb1

### 3

## 3.13

    find . -name "listener.ora"

    ./network/admin/samples/listener.ora

    find . -name "tnsnames.ora"

    ./network/admin/samples/tnsnames.ora

## 3.14

    ps -ef | grep "lstrctl"

    baherre+ 10342  9559  0 11:57 pts/4    00:00:00 grep --color=auto lstrctl

## 3.15


    SQL> ALTER PLUGGABLE DATABASE ORCLPDB1 OPEN READ WRITE;

    Pluggable database altered.

    
## 3.16

    rlwrap sqlplus sys as sysdba
    
    ici on a besoin de tapper le mot de passe

    le fichier utilisé est celui dans $ORACLE_HOME/network/admin/samples/tnsnames.ora

    l'utilisateur n'a pas les droits suffisant, on peut ajouter les permissions ou bien ajouter l'utilisateur dans le groupe sysoper pour bypass le mdp 